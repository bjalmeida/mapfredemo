var app=angular.module('app',['ngRoute', 'ui.bootstrap']);

app.factory('servicio',["$http","$q",function($http,$q){

	return {
		load : function(url){
			return $http.get(url)
			.then(
					function(response){
						return response;
					}, 
					function(errResponse){
						console.error('Error al crear.');
						return $q.reject(errResponse);
					}
			);
		}
	}
}]);

app.controller("ctrl",["$http","$scope","servicio",function($http,$scope,servicio){

	url_twitter="/Mapfre/demo/api/get/twitterFollowers";
	url_facebook="/Mapfre/demo/api/get/facebookLikes";

	url_sentiment="/Mapfre/demo/api/get/getSentiment?query='";
	url_volumen="/Mapfre/demo/api/get/getConversacion";

	url_comentarios="/Mapfre/demo/api/get/getComments?query='";
	
	 
		 $scope.paginationGNP = {
			currentPage: 1,
			maxSize: 5,
			totalItems: 50
		};
		
		$scope.paginationAxa= {
			currentPage: 1,
			maxSize: 5,
			totalItems :50
		};
		
		 $scope.paginationMapfre = {
			currentPage: 1,
			maxSize: 5,
			totalItems :50
		};

		 
	$scope.init=function(){
		servicio.load(url_twitter).then(function(info){
			angular.forEach(info.data, function(d, e) {
				d.fecha=$.format.date( new Date(d.fecha), 'dd/MM/yyyy');
			});
			$scope.current_twitter=info.data[info.data.length - 1];

		
			AmCharts.makeChart("chartdiv_twitter",
					{
				"type": "serial",
				"categoryField": "fecha",
				"chartCursor": {
					"enabled": true
				},
				"chartScrollbar": {
					"enabled": true
				},
				"graphs": [
				           {
				        	   "bullet": "round",
				        	   "title": "MAPFRE Mexico",
				        	   "lineColor": "#FF0000",
				        	   "valueField": "mapfre"
				           },
				           {
				        	   "bullet": "square",
				        	   "title": "Axa Seguros",
				        	   "lineColor": "#000000",
				        	   "valueField": "axa"
				           },
				           {
				        	   "bullet": "square",
				        	   "title": "GNP Seguros",
				        	   "lineColor": "#0000FF",
				        	   "valueField": "gnp"
				           }
				           ],
				           "valueAxes": [
				                         {
				                        	 "id": "ValueAxis-1",
				                        	 "title": "Seguidores"
				                         }
				                         ],
				                         "allLabels": [],
				                         "balloon": {},
				                         "legend": {
				                        	 "enabled": true,
				                        	 "useGraphSettings": true
				                         },
				                         "dataProvider": info.data
					});
		});


		servicio.load(url_volumen).then(function(info){
			angular.forEach(info.data, function(d, e) {
				d.fecha=$.format.date( new Date(d.fecha), 'dd/MM/yyyy');
			});

			AmCharts.makeChart("chartdiv_conversacion",
					{
				"type": "serial",
				"categoryField": "fecha",
				"chartCursor": {
					"enabled": true
				},
				"chartScrollbar": {
					"enabled": true
				},
				"graphs": [
				           {
				        	   "bullet": "round",
				        	   "title": "MAPFRE Mexico",
				        	   "lineColor": "#FF0000",
				        	   "valueField": "mapfre"
				           },
				           {
				        	   "bullet": "square",
				        	   "title": "Axa Seguros",
				        	   "lineColor": "#000000",
				        	   "valueField": "axa"
				           },
				           {
				        	   "bullet": "square",
				        	   "title": "GNP Seguros",
				        	   "lineColor": "#0000FF",
				        	   "valueField": "gnp"
				           }
				           ],
				           "valueAxes": [
				                         {
				                        	 "id": "ValueAxis-1",
				                        	 "title": "Seguidores"
				                         }
				                         ],
				                         "allLabels": [],
				                         "balloon": {},
				                         "legend": {
				                        	 "enabled": true,
				                        	 "useGraphSettings": true
				                         },
				                         "dataProvider": info.data
					});
		});


		servicio.load(url_facebook).then(function(info){
			angular.forEach(info.data, function(d, e) {
				d.fecha=$.format.date( new Date(d.fecha), 'dd/MM/yyyy');
			});
			$scope.current_facebook=info.data[info.data.length - 1];
			
			AmCharts.makeChart("chartdiv_facebook",
					{
				"type": "serial",
				"categoryField": "fecha",
				"chartCursor": {
					"enabled": true
				},
				"chartScrollbar": {
					"enabled": true
				},
				"graphs": [
				           {
				        	   "bullet": "round",
				        	   "title": "MAPFRE Mexico",
				        	   "lineColor": "#FF0000",
				        	   "valueField": "mapfre"
				           },
				           {
				        	   "bullet": "square",
				        	   "title": "Axa Seguros",
				        	   "lineColor": "#000000",
				        	   "valueField": "axa"
				           },
				           {
				        	   "bullet": "square",
				        	   "title": "GNP Seguros",
				        	   "lineColor": "#0000FF",
				        	   "valueField": "gnp"
				           }
				           ],
				           "valueAxes": [
				                         {
				                        	 "id": "ValueAxis-1",
				                        	 "title": "Seguidores"
				                         }
				                         ],
				                         "allLabels": [],
				                         "balloon": {},
				                         "legend": {
				                        	 "enabled": true,
				                        	 "useGraphSettings": true
				                         },
				                         "dataProvider": info.data
					});


		});

		servicio.load(url_sentiment+"@AXAMexico'").then(function(info){

			$scope.total_axa=0;

			angular.forEach(info.data, function(d, e) {
				$scope.total_axa+=d.column;
			});


			AmCharts.makeChart("chart_ev_netflix",
					{
				"type": "pie",
				"titleField": "sentiment",
				"valueField": "column",
				"colorField": "color",
				"labelText": "",
				"legend": {
					"enabled": true,
					"align": "center",
					"markerType": "circle"
				},
				"dataProvider": info.data
					}
			);
		});


		servicio.load(url_sentiment+"@MAPFRE_MX'").then(function(info){

			$scope.total_mapfre=0;

			angular.forEach(info.data, function(d, e) {
				$scope.total_mapfre+=d.column;
			});


			AmCharts.makeChart("chart_ev_claro",
					{
				"type": "pie",
				"labelText": "",
				"titleField": "sentiment",
				"valueField": "column",
				"colorField": "color",
				"legend": {
					"enabled": true,
					"align": "center",
					"markerType": "circle"
				},
				"dataProvider": info.data
					}
			);
		});

		servicio.load(url_sentiment+"@GNPSeguros'").then(function(info){

			$scope.total_gnp=0;

			angular.forEach(info.data, function(d, e) {
				$scope.total_gnp+=d.column;
				 
			});


			AmCharts.makeChart("chart_ev_blim",
					{
				"type": "pie",
				"labelText": "",
				"titleField": "sentiment",
				"valueField": "column",
				"colorField": "color",
				"legend": {
					"enabled": true,
					"align": "center",
					"markerType": "circle"
				},
				"dataProvider": info.data
					}
			);
		});

		//paginacion 
		
	 
 	
		servicio.load(url_comentarios+"@mapfre_mx'&page="+$scope.paginationMapfre.currentPage).then(function(info){
			$scope.comentariosMapfre=info.data.content;
			 $scope.paginationMapfre.totalItems = info.data.totalElements;
			
		});

		servicio.load(url_comentarios+"@gnpseguros'&page="+$scope.paginationGNP.currentPage).then(function(info){
			$scope.comentariosGNP=info.data.content;
			 $scope.paginationGNP.totalItems = info.data.totalElements;
		});

		servicio.load(url_comentarios+"@axamexico'&page="+$scope.paginationAxa.currentPage).then(function(info){

			$scope.comentariosAxa=info.data.content;
			 $scope.paginationAxa.totalItems = info.data.totalElements;

		});

		  
		$scope.pageChangedMapfre = function(){
			servicio.load(url_comentarios+"@mapfre_mx'&page="+$scope.paginationMapfre.currentPage).then(function(info){
				$scope.comentariosMapfre=info.data.content;
			});
		}
		
		$scope.pageChangedAxa = function(){
			servicio.load(url_comentarios+"@axamexico'&page="+$scope.paginationAxa.currentPage ).then(function(info){
				$scope.comentariosAxa=info.data.content;

			});
		}
		
		$scope.pageChangedGNP = function(){
			servicio.load(url_comentarios+"@gnpseguros'&page="+$scope.paginationGNP.currentPage).then(function(info){
				$scope.comentariosGNP=info.data.content;
			});
		}
		
		 console.log( $scope.paginationMapfre);

	}

}]);



