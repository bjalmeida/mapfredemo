 


<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SB Admin</title>
<script
	src='${pageContext.request.contextPath}/resources/plugins/jquery/dist/jquery.min.js'></script>
<script
	src='${pageContext.request.contextPath}/resources/plugins/date/jquery-dateFormat.js'></script>
<!-- Bootstrap Core CSS -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Timeline CSS -->
<link
	href="${pageContext.request.contextPath}/resources/dist/css/timeline.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="${pageContext.request.contextPath}/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script
	src='${pageContext.request.contextPath}/resources/plugins/angular/angular.min.js'></script>
<script
	src='${pageContext.request.contextPath}/resources/plugins/angular/angular-animate.min.js'></script>
<script
	src='${pageContext.request.contextPath}/resources/plugins/angular/angular-route.min.js'></script>
<script
	src='${pageContext.request.contextPath}/resources/plugins/angular/angular-sanitize.min.js'></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-resource.min.js"></script>
<body ng-controller="ctrl" ng-init="init()">

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/ClaroVideo">Demo Seguimiento
					MAPFRE M�xico</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
						<i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#"><i class="fa fa-user fa-fw"></i> User
								Profile</a></li>
						<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
						</li>
						<li class="divider"></li>
						<li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-sign-out fa-fw"></i>
								Logout</a></li>
					</ul> <!-- /.dropdown-user --></li>
				<!-- /.dropdown -->
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li><a href="/"><i class="fa fa-dashboard fa-fw"></i>
								Dashboard</a></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<div style="padding: 15px 15px 15px 15px;">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Dashboard</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="panel panel-red">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-6">
									<div class="huge">MAPFRE M�xico</div>
									<i class="fa fa-clipboard  fa-3x"></i>
								</div>
								<div class="col-xs-6 text-right">
									<h4 ng-bind="current_twitter.mapfre"></h4>
									<div>Seguidores (Twitter)</div>
									<h4 ng-bind="current_facebook.mapfre"></h4>
									<div>Likes (Facebook)</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-6">
									<div class="huge">Axa Seguros</div>
									<i class="fa fa-clipboard  fa-3x"></i>
								</div>
								<div class="col-xs-6 text-right">
									<h4 ng-bind="current_twitter.axa"></h4>
									<div>Seguidores (Twitter)</div>
									<h4 ng-bind="current_facebook.axa"></h4>
									<div>Likes (Facebook)</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-6">
									<div class="huge">GNP Seguros</div>
									<i class="fa fa-clipboard  fa-3x"></i>
								</div>
								<div class="col-xs-6 text-right">
									<h4 ng-bind="current_twitter.gnp"></h4>
									<div>Seguidores (Twitter)</div>
									<h4 ng-bind="current_facebook.gnp"></h4>
									<div>Likes (Facebook)</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i> Evolucion de seguidores
							de Twitter
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div id="chartdiv_twitter"
								style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
				<!-- /.col-lg-8 -->
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i> Evolucion de likes de
							facebook
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div id="chartdiv_facebook"
								style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
				<!-- /.col-lg-4 -->
			</div>
			<!-- /.row -->
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i> Evaluaci�n para MAPFRE M�xico
						</div>
						<div class="panel-body">
							<div id="chart_ev_claro"
								style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i> Evaluaci�n para Axa Seguros
						</div>
						<div class="panel-body">
							<div id="chart_ev_netflix"
								style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i> Evaluaci�n para GNP Seguros
						</div>
						<div class="panel-body">
							<div id="chart_ev_blim"
								style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-red">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-8">
									<i class="fa fa-comments-o fa-2x"></i><span>
										&nbsp;Numero de comentarios<span>
								</div>
								<div class="col-md-4">
									<h4 ng-bind="total_mapfre"></h4>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-8">
									<i class="fa fa-comments-o fa-2x"></i><span>
										&nbsp;Numero de comentarios<span>
								</div>
								<div class="col-md-4">
									<h4 ng-bind="total_axa"></h4>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-8">
									<i class="fa fa-comments-o fa-2x"></i><span>
										&nbsp;Numero de comentarios<span>
								</div>
								<div class="col-md-4">
									<h4 ng-bind="total_gnp"></h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-comments-o fa-fw"></i> Comentarios MAPFRE M�xico
						</div>
						<div class="panel-body"
							style="height: 40em; overflow: scroll; line-height: 1em">
							<div class="row" ng-repeat="cb in comentariosMapfre">
								<div class="col-md-4">
									<H4>{{cb.username}}</H4>
									<img src="{{cb.avatar}}"></img>
								</div>
								<div class="col-md-7" style="padding: 15px 15px 15px 15px;">
									<p>{{cb.text}}</p>
								</div>
								<div class="col-md-1" style="height: 100px;"
									ng-style="{'background-color': cb.backgroundCol}"></div>
							</div>
						</div>
						<!-- /.panel-body -->
						<div class="row text-center style="margin: 0px;"> 
							<uib-pagination total-items="paginationMapfre.totalItems"
					ng-model="paginationMapfre.currentPage" max-size="paginationMapfre.maxSize" class="pagination-sm"
					boundary-links="true" ng-change="pageChangedMapfre()" id="pagMapfre"
					first-text="&laquo;" previous-text="&lsaquo;" next-text="&rsaquo;"
					last-text="&raquo;" style="margin-bottom: 5px; margin-top: 5px;"></uib-pagination>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-comments-o fa-fw"></i> Comentarios Axa Seguros
						</div>
						<div class="panel-body"
							style="height: 40em; overflow: scroll; line-height: 1em">
							<div class="row" ng-repeat="cb in comentariosAxa">
								<div class="col-md-4">
									<H4>{{cb.username}}</H4>
									<img src="{{cb.avatar}}"></img>
								</div>
								<div class="col-md-7" style="padding: 15px 15px 15px 15px;">
									<p>{{cb.text}}</p>
								</div>
								<div class="col-md-1" style="height: 100px;"
									ng-style="{'background-color': cb.backgroundCol}"></div>
							</div>
						</div>
						<!-- /.panel-body -->
						<div class="row text-center style="margin: 0px;"> 
							<uib-pagination total-items="paginationAxa.totalItems"
					ng-model="paginationAxa.currentPage" max-size="paginationAxa.maxSize" class="pagination-sm"
					boundary-links="true" ng-change="pageChangedAxa()" id="pagAxa"
					first-text="&laquo;" previous-text="&lsaquo;" next-text="&rsaquo;"
					last-text="&raquo;" style="margin-bottom: 5px; margin-top: 5px;"></uib-pagination>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-comments-o fa-fw"></i> Comentarios GNP Seguros
						</div>
						<div class="panel-body"
							style="height: 40em; overflow: scroll; line-height: 1em">
							<div class="row" ng-repeat="cb in comentariosGNP">
								<div class="col-md-4">
									<H4>{{cb.username}}</H4>
									<img src="{{cb.avatar}}"></img>
								</div>
								<div class="col-md-7" style="padding: 15px 15px 15px 15px;">
									<p>{{cb.text}}</p>
								</div>
								<div class="col-md-1" style="height: 100px;"
									ng-style="{'background-color': cb.backgroundCol}"></div>
							</div>
						</div>
					<!-- /.panel-body -->
						<div class="row text-center style="margin: 0px;"> 
							<uib-pagination total-items="paginationGNP.totalItems"
					ng-model="paginationGNP.currentPage" max-size="paginationGNP.maxSize" class="pagination-sm"
					boundary-links="true" ng-change="pageChangedGNP()" id="pagGNP"
					first-text="&laquo;" previous-text="&lsaquo;" next-text="&rsaquo;"
					last-text="&raquo;" style="margin-bottom: 5px; margin-top: 5px;"></uib-pagination>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i> Seguimiento de la
							conversaci�n
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div id="chartdiv_conversacion"
								style="width: 100%; height: 400px; background-color: #FFFFFF"></div>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->
	</div>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/metisMenu/dist/metisMenu.min.js"></script>

	<!-- paginacion -->
	<script
		src="${pageContext.request.contextPath}/resources/js/ui-bootstrap-tpls.js"></script>
	
	<!-- Custom Theme JavaScript -->
	<script
		src="${pageContext.request.contextPath}/resources/js/dashboard.js"></script>
	<!-- amCharts javascript sources -->
	<script type="text/javascript"
		src="http://www.amcharts.com/lib/3/amcharts.js"></script>
	<script type="text/javascript"
		src="http://www.amcharts.com/lib/3/serial.js"></script>
	<script type="text/javascript"
		src="http://www.amcharts.com/lib/3/pie.js"></script>
</body>

</html>
