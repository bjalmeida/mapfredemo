 
package com.oeste.demo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.oeste.demo.model.Followers;
import com.oeste.demo.model.Likes;
import com.oeste.demo.model.TwitterComments;
import com.oeste.demo.repo.FacebookHistoryRepository;
import com.oeste.demo.repo.TwitterHistoryFollowers;
import com.oeste.demo.repo.TwitterRepository;


@Controller
@RequestMapping("/demo/api")
public class DemoAPIController {

	private static final Logger logger = LoggerFactory.getLogger(DemoAPIController.class);
	
	@Autowired 
	FacebookHistoryRepository facebookHistoryRepository;

	@Autowired
	TwitterHistoryFollowers twitterHistoryFollowers;
	
	@Autowired
	TwitterRepository twitterRepository;
 

	public static String ISO_INSTANT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

	@RequestMapping(value = "get/facebookLikes", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<Map<String,Object>>> getfacebookLikes() {

		List<Map<String,Object>> response=new ArrayList<>();
		boolean axa=false;
		boolean gnp=false;
		boolean mapfre=false;
		HashMap<String, Object> n=new HashMap<String,Object>();
		/*CORREGIR QUERY*/
		for (Likes l: facebookHistoryRepository.findByQuery("mapfremx,axamexico,gnpsegurosoficial",new Sort(Sort.Direction.ASC,("timestamp")))) {
		 			 
			switch (l.getQuery().toLowerCase()){
			case "axamexico":			
				axa=true;
				n.put("axa", l.getLikes());
				break;
			case "mapfremx":
				mapfre=true;
				n.put("mapfre", l.getLikes());
				break;

			case "gnpsegurosoficial":
				gnp=true;
				n.put("gnp", l.getLikes());
				break;
			}

			if(axa&gnp&mapfre){

				axa=false;
				mapfre=false;
				gnp=false;

				n.put("fecha", l.getTimestamp());
				response.add(n);

				n=new HashMap<String,Object>();
			}
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "get/twitterFollowers", method = RequestMethod.GET, produces = { "application/json" })
 public ResponseEntity<List<Map<String,Object>>> getTwitterFollowers() {

		List<Map<String,Object>> response=new ArrayList<>();
		boolean axa=false;
		boolean gnp=false;
		boolean mapfre=false;
		HashMap<String, Object> n=new HashMap<String,Object>();
		Iterable<Followers> follow=twitterHistoryFollowers.findAll();

		for (Followers l: follow) {

			switch (l.getQuery()){
			case "@AXAMexico":
				axa=true;
				n.put("axa", l.getFollowers());
				break;
			case "@MAPFRE_MX":
				mapfre=true;
				n.put("mapfre", l.getFollowers());
				break;

			case "@GNPSeguros":
				gnp=true;
				n.put("gnp", l.getFollowers());
				break;
			}

			if(axa&gnp&mapfre){

				axa=false;
				mapfre=false;
				gnp=false;

				n.put("fecha", l.getTimestamp());
				response.add(n);

				n=new HashMap<String,Object>();
			}
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "get/getSentiment", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<Map<String, Object>>> getSentiment(@RequestParam("query") String query) {

		List<Map<String,Object>> response=new ArrayList<>();
		HashMap<String, Object> n=new HashMap<String,Object>();

		for (int i=0;i<3 ;i++) {
			
			int val=twitterRepository.findByQueryAndSentiment(query, i).size();
			
			
			switch (i){
			case 0:
				n.put("sentiment", "Neutro");
				n.put("color", "#DEB887");
				break;
			case 1:
				n.put("sentiment", "Positivo");
				n.put("color", "#008000");
				break;
			case 2:
				n.put("sentiment", "Negativo");
				n.put("color", "#FF0000");
				break;
			}
			n.put("column", val);
			response.add(n);
			n=new HashMap<>();
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "get/getConversacion", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<Map<String, Object>>> getConversacion() {

		List<Map<String,Object>> response=new ArrayList<>();
		HashMap<String, Object> n=new HashMap<String,Object>();

		String iso_inicial="2016-10-31T00:00:00Z";

		DateFormat dateFormat = new SimpleDateFormat(ISO_INSTANT);
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");

		try {
			Date d_inicial=dateFormat.parse(iso_inicial);
			do{

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(d_inicial);
				calendar.add(Calendar.HOUR, 24); 

				System.out.println("Inicial:"+d_inicial+" Final:"+calendar.getTime());
 
				n.put("gnp",twitterRepository.findByAnnotatedCreatedAtAndQuery(d_inicial,calendar.getTime(), "@gnpseguros").size());
				n.put("axa",twitterRepository.findByAnnotatedCreatedAtAndQuery(d_inicial, calendar.getTime(), "@axamexico").size());
				n.put("mapfre",twitterRepository.findByAnnotatedCreatedAtAndQuery(d_inicial, calendar.getTime(), "@mapfre_mx").size());
				n.put("fecha",dateFormat.format(d_inicial));
				response.add(n);

				calendar.add(Calendar.HOUR, -24); 
				calendar.add(Calendar.DAY_OF_YEAR, 1);  

				d_inicial=calendar.getTime();

				n=new HashMap<String,Object>();

			}while(!(fmt.format(d_inicial).equals(fmt.format(new Date()))));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "get/getComments", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<Page<TwitterComments>> getComments(@RequestParam("query") String query,@RequestParam(value="page") int noPage) {
	
		Sort sort = new Sort(Sort.Direction.DESC, "createdAt"); 
		Page<TwitterComments> response = twitterRepository.finndByCuenta(query, new PageRequest(noPage-1, 10,sort));
 
		for (TwitterComments e : response) {
			try{
				if(e.getSentiment()==0){
					e.setBackgroundCol("#DEB887");
				}else if(e.getSentiment()==1){
					e.setBackgroundCol("#008000");
				}else{
					e.setBackgroundCol("#FF0000");
				}
			}catch (NullPointerException e2) {
				e.setBackgroundCol("#DEB887");
			}
		}
		return new ResponseEntity<>(response,HttpStatus.OK);

	}

}
