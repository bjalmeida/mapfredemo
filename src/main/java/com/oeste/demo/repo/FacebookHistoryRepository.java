package com.oeste.demo.repo;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import com.oeste.demo.model.Likes;
import java.lang.String;
import java.util.List;

public interface FacebookHistoryRepository extends CrudRepository<Likes, String>{
	List<Likes> findByQuery(String query, Sort s);
}
