package com.oeste.demo.repo;


import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import com.oeste.demo.model.Followers;
import java.lang.String;

public interface TwitterHistoryFollowers extends CrudRepository<Followers, String>{
		List<Followers> findByQuery(String user,Sort s);
}
