package com.oeste.demo.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.solr.repository.Query;

import com.oeste.demo.model.TwitterComments; 

public interface TwitterRepository extends CrudRepository<TwitterComments, String>{
	
 	List<TwitterComments> findByQueryAndSentiment(String query,Integer sentiment);
	
	@Query("createdAt:[?0 TO ?1] AND _query_:?2")
	List<TwitterComments> findByAnnotatedCreatedAtAndQuery(Date inicio,Date fin,String query);
	
	Page<TwitterComments> findTop10ByQueryOrderByCreatedAtDesc(String query, Pageable pageable);
	
	@Query(value="*:*", filters={"_query_:?0"})
	Page<TwitterComments> finndByCuenta(String query, Pageable pageable);
}
