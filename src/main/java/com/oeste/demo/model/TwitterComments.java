package com.oeste.demo.model;

import java.util.Date;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(solrCoreName="twitter")
public class TwitterComments {
	
	@Id
	@Field
	String idOnTwitter;
	@Field
	String username;
	@Field
    Date createdAt;
	@Field
    String avatar;
	@Field
    Integer sentiment;
	@Field
	String text;
    
    @Field(value="_query_")
    String query;
    
    String backgroundCol;

	public String getIdOnTwitter() {
		return idOnTwitter;
	}

	public void setIdOnTwitter(String idOnTwitter) {
		this.idOnTwitter = idOnTwitter;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}


	public Integer getSentiment() {
		return sentiment;
	}

	public void setSentiment(Integer sentiment) {
		this.sentiment = sentiment;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getBackgroundCol() {
		return backgroundCol;
	}

	public void setBackgroundCol(String backgroundCol) {
		this.backgroundCol = backgroundCol;
	}
    

}
